package com.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoRepeat {
    /**
     * 过期时间,默认3
     * @return expire
     */
    int expire() default 3;


    /**
     * 注解的动态参数,传入的redisKey
     * @return redisKey
     */
    String redisKey() default "";

    /**
     * 获取时间单位,默认是秒
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;
}
