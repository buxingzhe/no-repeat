package com.example.condition;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class QueryCondition {

    @Schema( name = "编号")
    private String id;

    @Schema(name = "姓名")
    private String name;

    @Schema(name = "唯一键")
    private String redisKey;
}
