package com.example.controller;

import com.example.annotation.NoRepeat;
import com.example.condition.QueryCondition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;


@RestController
@Tag(name = "防重复提交接口")
public class DemoController {

    /**
     * @param condition 参数
     * @param redisKey 数据用来标识唯一行的key,我们用来作为redis的锁，由前端传入,也可以不传
     */
    @PostMapping("/submitRepeat")
    @NoRepeat(redisKey = "#redisKey",expire = 5)
    @Operation(summary = "重复提交接口",description = "测试重复提交")
    public String submitRepeat(@RequestBody QueryCondition condition, @RequestParam("redisKey") String redisKey) {
        condition.setRedisKey(redisKey);
        return condition.toString();
    }
}
